﻿using System;
using Caliburn.Micro.Contrib.Decorators;
using MSMQHelper.Models;
using MSMQHelper.Services;
using MSMQHelper.ViewModels;
using NUnit.Framework;

namespace MSMQHelper.Tests
{
    [TestFixture]
    public class TestPopulateDialogViewModel
    {
        [Test]
        public void WhenAssemblyFileIsNotSuppliedThenExpectException()
        {
            var x = new PopulateDialogViewModel(new UserSettings())
                        {SelectedType = typeof (QueueDefinition).FullName, SelectedVersion = new NServiceBusVersion("2.0.0.0") };
            foreach(var result in x.Select())
            {
                var args = result.BlockingExecute();
                Assert.IsTrue(args.Error is NullReferenceException);
            }
        }
    }
}
