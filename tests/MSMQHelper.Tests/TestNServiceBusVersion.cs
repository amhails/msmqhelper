﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSMQHelper.Services;
using NUnit.Framework;

namespace MSMQHelper.Tests
{
    [TestFixture]
    public class TestNServiceBusVersion
    {
        [Test]
        public void WhenGetVersionsReturnsCorrectly()
        {
            var versions = NServiceBusVersion.GetAvailableVersions().ToArray();
            Assert.AreEqual(3, versions.Count());
            Assert.AreEqual("2.0.0.0", versions[0].ToString());
            Assert.AreEqual("2.6.0.0", versions[1].ToString());
            Assert.AreEqual("2.6.0.1511", versions[2].ToString());
        }
    }
}
