﻿using System;
using MSMQHelper.Services;
using NUnit.Framework;

namespace MSMQHelper.Tests
{
    [TestFixture]
    public class TestNServiceBusSerializer
    {
        [Test]
        public void WhenTypeHasNoDefaultConstructorThenStillSerializeSuccessfully()
        {
            var xml =
                new NServiceBusHelper(new NServiceBusVersion("2.0.0.0")).Serialize(
                    typeof (MessageWithNoDefaultConstructor).Assembly.Location,
                    typeof (MessageWithNoDefaultConstructor).FullName);

            Assert.IsNotNullOrEmpty(xml);
        }

        [Test]
        public void WhenTypeIsNotIMessageThenExpectException()
        {
            var exception =
                Assert.Throws<ArgumentException>(
                    () =>
                    new NServiceBusHelper(new NServiceBusVersion("2.0.0.0")).Serialize(
                        typeof (SimpleType).Assembly.Location,
                        typeof (SimpleType).FullName));

            Assert.AreEqual("Message type does not support IMessage for this version of NServiceBus", exception.Message);
        }

        [Test]
        [Ignore("Not working currently as will attempt to load NSB 2.0 from bin folder first")]
        public void WhenTypeIsNotCorrectVersionThenExpectException()
        {
            var exception =
                Assert.Throws<ArgumentException>(
                    () => new NServiceBusHelper(new NServiceBusVersion("2.6.0.0")).Serialize(
                        typeof (SimpleMessage).Assembly.Location,
                        typeof (SimpleMessage).FullName));

            Assert.AreEqual("Message type does not support IMessage for this version of NServiceBus", exception.Message);
        }

        [Test]
        public void WhenTypeIsCorrectVersionThenSerializeSuccesfully()
        {
            var xml = new NServiceBusHelper(new NServiceBusVersion("2.0.0.0")).Serialize(
                typeof (SimpleMessage).Assembly.Location,
                typeof (SimpleMessage).FullName);

            Assert.IsNotNullOrEmpty(xml);
        }

        [Test]
        public void WhenTypeContainsEncryptedStringThenSerializeSuccessfully()
        {
            var xml = new NServiceBusHelper(new NServiceBusVersion("2.0.0.0")).Serialize(
                typeof (MessageWithEncryptedString).Assembly.Location,
                typeof (MessageWithEncryptedString).FullName);
            Assert.IsNotNullOrEmpty(xml);
        }
    }
}