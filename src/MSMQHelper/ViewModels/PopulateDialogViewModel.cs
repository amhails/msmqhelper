﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Caliburn.Micro;
using Caliburn.Micro.Contrib.Dialogs;
using Caliburn.Micro.Contrib.Results;
using MSMQHelper.Models;
using MSMQHelper.Services;
using Microsoft.Win32;
using Caliburn.Micro.Contrib;

namespace MSMQHelper.ViewModels
{
    public class PopulateDialogViewModel : Screen
    {
        private readonly UserSettings settings;
        private string selectedType;
        private string[] types;
        private Sandbox sandbox;

        public PopulateDialogViewModel(UserSettings settings)
        {
            this.settings = settings;
            this.DisplayName = "Select Message Type";
        }

        protected override void OnActivate()
        {
            RefreshTypes(false);
        }

        protected override void OnDeactivate(bool close)
        {
            if(sandbox != null)
                sandbox.Dispose();
        }

        public NServiceBusHelper NServiceBusHelper
        {
            get
            {
                if (sandbox == null)
                {
                    sandbox = Sandbox.Create("serializer", SelectedVersion.GetPath());
                }
                return sandbox.InjectRemoteObject<NServiceBusHelper>(new [] { SelectedVersion });
            }
        }


        public string AssemblyFile
        {
            get { return settings.Assembly; }
            set
            {
                settings.Assembly = value;
                NotifyOfPropertyChange(() => AssemblyFile);
                RefreshTypes(false);
            }
        }

        public string[] Types
        {
            get { return types; }
            set
            {
                types = value;
                NotifyOfPropertyChange(() => Types);
            }
        }

        public string EncryptionKey
        {
            get { return settings.EncryptionKey; }
            set
            {
                settings.EncryptionKey = value;
                NotifyOfPropertyChange(() => EncryptionKey);
            }
        }

        public string SelectedType
        {
            get { return selectedType; }
            set
            {
                selectedType = value;
                NotifyOfPropertyChange(() => SelectedType);
                NotifyOfPropertyChange(() => CanSelect);
            }
        }

        public NServiceBusVersion[] Versions
        {
            get { return NServiceBusVersion.GetAvailableVersions().ToArray(); }
        }

        public NServiceBusVersion SelectedVersion
        {
            get { return settings.NServiceBusVersion; }
            set
            {
                settings.NServiceBusVersion = value;
                NotifyOfPropertyChange(() => SelectedVersion);
                RefreshTypes(true);
            }
        }

        public string Xml { get; set; }

        public bool CanSelect
        {
            get { return SelectedType != null; }
        }

        public IEnumerable<IResult> Select()
        {
            var serializeMessage = new DelegateResult(() => { Xml = GetXml(); });
            serializeMessage.Completed += (s, e) =>
                                          {
                                              if (e.Error == null)
                                              {
                                                  TryClose();
                                              }
                                          };
            yield return serializeMessage.Rescue().Execute(e => new DialogResult<Answer>(new Error(e.Message)).AsCoroutine());
        }

        public void Cancel()
        {
            Xml = "";

            TryClose();
        }

        private string GetXml()
        {
            return NServiceBusHelper.Serialize(AssemblyFile, SelectedType, EncryptionKey);
        }

        public void OpenFileDialog()
        {
            var fileDialog = new OpenFileDialog {Filter = "dll files (*.dll)|*.dll|exe files (*.exe)|*.exe", Multiselect = false};
            fileDialog.ShowDialog();

            if (!string.IsNullOrEmpty(fileDialog.FileName))
            {
                AssemblyFile = fileDialog.FileName;
            }
        }

        private void RefreshTypes(bool changeVersion)
        {
            if (changeVersion && sandbox != null)
            {
                sandbox.Dispose();
                sandbox = null;
            }

            Types = null;
            if (SelectedVersion != null && !string.IsNullOrEmpty(AssemblyFile) && File.Exists(AssemblyFile))
            {
                try
                {
                    Types = NServiceBusHelper.ListMessageTypes(AssemblyFile);
                }
                catch (Exception e)
                {
                    Run.Coroutine(new DialogResult<Answer>(new Error(e.Message)));
                }
            }
        }
    }
}