﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using System.Xml;
using System.Xml.Linq;
using Caliburn.Micro;
using Caliburn.Micro.Contrib.Dialogs;
using Caliburn.Micro.Contrib.Results;
using MSMQHelper.Models;
using MSMQHelper.Services;
using Caliburn.Micro.Contrib;

namespace MSMQHelper.ViewModels
{
    public class SendMessageViewModel : Screen, IDataErrorInfo
    {
        private readonly QueueDefinition queueDefinition;
        private readonly MessageDefinition messageDefinition;
        private readonly IMSMQService service;
        private readonly IWindowManager windowManager;
        private string status = "Ready";

        public SendMessageViewModel(QueueDefinition queueDefinition, MessageDefinition messageDefinition,
                                    IMSMQService service, IWindowManager windowManager)
        {
            this.queueDefinition = queueDefinition;
            this.messageDefinition = messageDefinition;
            this.service = service;
            this.windowManager = windowManager;

            this.DisplayName = "Send Message";
        }

        public string ServerName
        {
            get { return queueDefinition.ServerName; }
            set
            {
                queueDefinition.ServerName = value;
                NotifyOfPropertyChange(() => ServerName);
                NotifyOfPropertyChange(() => QueueName);
                NotifyOfPropertyChange(() => CanSendMessage);
            }
        }

        public string QueueName
        {
            get { return queueDefinition.QueueName; }
            set
            {
                queueDefinition.QueueName = value;
                NotifyOfPropertyChange(() => ServerName);
                NotifyOfPropertyChange(() => QueueName);
                NotifyOfPropertyChange(() => CanSendMessage);
            }
        }

        public string Label
        {
            get { return messageDefinition.Label; }
            set
            {
                messageDefinition.Label = value;
                NotifyOfPropertyChange(() => Label);
                NotifyOfPropertyChange(() => CanSendMessage);
            }
        }

        public string Body
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(messageDefinition.Body))
                {
                    XDocument body = null;
                    try
                    {
                        body = XDocument.Parse(messageDefinition.Body);
                    }
                    catch (XmlException)
                    {
                    }

                    if (body != null)
                    {
                        var sb = new StringBuilder();
                        var settings = new XmlWriterSettings {Indent = true};
                        using (var writer = XmlWriter.Create(sb, settings))
                        {
                            body.WriteTo(writer);
                        }

                        return sb.ToString();
                    }
                }
                return messageDefinition.Body;
            }
            set
            {
                messageDefinition.Body = value;
                NotifyOfPropertyChange(() => Body);
                NotifyOfPropertyChange(() => CanSendMessage);
            }
        }

        public bool IsRecoverable
        {
            get { return messageDefinition.IsRecoverable; }
            set
            {
                messageDefinition.IsRecoverable = value;
                NotifyOfPropertyChange(() => IsRecoverable);
            }
        }

        public string Status
        {
            get { return status; }
            set
            {
                status = value;
                NotifyOfPropertyChange(() => Status);
            }
        }

        #region Send Message

        public IEnumerable<IResult> SendMessage()
        {
            var sendMessage = new DelegateResult(() => service.SendMessage(queueDefinition, messageDefinition));
            sendMessage.Completed += (s, a) => { Status = a.Error == null ? "Send successful" : "Send failed"; };

            yield return sendMessage.Rescue().Execute(e => new DialogResult<Answer>(new Error(e.Message)).AsCoroutine());
        }

        public bool CanSendMessage
        {
            get
            {
                return queueDefinition.IsValid() && messageDefinition.IsValid(); 
            }
        }

        #endregion

        #region Populate Body

        public void OpenPopulateDialog()
        {
            var populateModel = IoC.Get<PopulateDialogViewModel>();
            windowManager.ShowDialog(populateModel);

            if (!string.IsNullOrEmpty(populateModel.Xml))
                Body = populateModel.Xml;
        }

        #endregion

        protected override void OnDeactivate(bool close)
        {
            if (close)
                CustomSettings.Default.Save();
        }

        string IDataErrorInfo.this[string propertyName]
        {
            get
            {
                string error = null;
                if (propertyName == "QueueName" || propertyName == "ServerName")
                {
                    error = (queueDefinition as IDataErrorInfo)[propertyName];
                }
                if (propertyName == "Body" || propertyName == "Label")
                    error = (messageDefinition as IDataErrorInfo)[propertyName];

                CommandManager.InvalidateRequerySuggested();
                return error;
            }
        }

        string IDataErrorInfo.Error
        {
            get { return null; }
        }
    }
}