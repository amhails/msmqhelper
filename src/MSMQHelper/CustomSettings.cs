﻿using System.Configuration;
using MSMQHelper.Models;
using MSMQHelper.Services;

namespace MSMQHelper
{
    public class CustomSettings : ApplicationSettingsBase
    {
        private static readonly CustomSettings defaultInstance = ((CustomSettings)(Synchronized(new CustomSettings())));
        
        public static CustomSettings Default 
        {
            get { return defaultInstance; }
        }

        [UserScopedSetting]
        public UserSettings UserSettings
        {
            get
            {
                if(this["UserSettings"] == null)
                    this["UserSettings"] = new UserSettings() { QueueDefinition = new QueueDefinition(), MessageDefinition = new MessageDefinition(), EncryptionKey = "gdDbqRpqdRbTs3mhdZh9qCaDaxJXl+e6" };
                return ((UserSettings)(this["UserSettings"]));
            }
            set
            {
                this["UserSettings"] = value;
            }
        }
    }
}
