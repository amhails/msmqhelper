﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;

namespace MSMQHelper.Services
{
    public class NServiceBusHelper : MarshalByRefObject
    {
        private readonly NServiceBusVersion version;
        private Dictionary<string, Assembly> loadedAssemblies = new Dictionary<string, Assembly>();

        private const string MessageMapper = "NServiceBus.MessageInterfaces.MessageMapper.Reflection.MessageMapper";
        private const string XmlSerializer = "NServiceBus.Serializers.XML.MessageSerializer";
        private const string EncryptionService = "NServiceBus.Encryption.Rijndael.EncryptionService";
        private const string MessageType = "NServiceBus.IMessage";
        private const string EncryptedString = "NServiceBus.WireEncryptedString";
        private const string EncryptedValue = "NServiceBus.Encryption.EncryptedValue";

        public NServiceBusHelper(NServiceBusVersion version)
        {
            this.version = version;
        }

        #region Private methods
        
        private Assembly LoadNServiceBus()
        {
            var assemblyName = string.Format(@"NServiceBus, Version={0}, Culture=neutral, PublicKeyToken=9fc386479f8a226c", version);
            return LoadAssembly(assemblyName);
        }

        private Assembly LoadNServiceBusCore()
        {
            var assemblyName = string.Format(@"NServiceBus.Core, Version={0}, Culture=neutral, PublicKeyToken=9fc386479f8a226c", version);
            return LoadAssembly(assemblyName);
        }

        private Assembly LoadAssembly(string assemblyName)
        {
            try
            {
                return Assembly.Load(assemblyName);
            }
            catch (IOException)
            {
                throw new NotImplementedException(string.Format("NServiceBus version {0} not found", version));
            }
        }

        private Type GetNServiceBusType(Assembly nsbAssembly, string typeName)
        {
            var nsbType = nsbAssembly.GetType(typeName);
            if (nsbType == null)
                throw new Exception(string.Format("Could not get NServiceBus type {0}", typeName));
            return nsbType;
        }

        #endregion

        public string[] ListMessageTypes(string assemblyFile)
        {
            if (string.IsNullOrEmpty(assemblyFile))
                throw new ArgumentNullException("assemblyFile");
            if (!File.Exists(assemblyFile))
                throw new FileNotFoundException("Assembly file could not be found");

            var messageTypeAssembly = Assembly.LoadFrom(assemblyFile);

            var nsb = LoadNServiceBus();
            var nsbMessageType = GetNServiceBusType(nsb, MessageType);

            return (from type in messageTypeAssembly.GetTypes()
                    where !type.IsGenericType && !type.IsGenericTypeDefinition && type.IsClass && !type.IsNested && nsbMessageType.IsAssignableFrom(type)
                    orderby type.FullName
                    select type.FullName).ToArray();
        }

        public string Serialize(string assemblyFile, string type, string encryptionKey = "gdDbqRpqdRbTs3mhdZh9qCaDaxJXl+e6")
        {
            var messageTypeAssembly = Assembly.LoadFrom(assemblyFile);
            Type messageType = messageTypeAssembly.GetType(type);
            dynamic message = ConstructMessageObject(messageType);

            var nsb = LoadNServiceBus();
            var nsbCore = LoadNServiceBusCore();
            
            var interfaces = messageType.GetInterfaces();
            var nsbMessageType = nsb.GetType(MessageType);
            if (!interfaces.Any(t => t == nsbMessageType))
                throw new ArgumentException("Message type does not support IMessage for this version of NServiceBus");

            dynamic serializer = Activator.CreateInstance(GetNServiceBusType(nsbCore, XmlSerializer));
            
            dynamic messageMapper = Activator.CreateInstance(GetNServiceBusType(nsbCore, MessageMapper));
            dynamic encryptionService = Activator.CreateInstance(GetNServiceBusType(nsbCore, EncryptionService));
            encryptionService.Key = Encoding.ASCII.GetBytes(encryptionKey);

            serializer.EncryptionService = encryptionService;
            serializer.MessageMapper = messageMapper;
            serializer.InitType(message.GetType());
            serializer.InitType(GetNServiceBusType(nsbCore, EncryptedValue));

            var nsbEncryptedString = GetNServiceBusType(nsb, EncryptedString);
            PopulateMessageObject(message, nsbEncryptedString);

            var myGenericType = typeof(List<>).MakeGenericType(new[] { messageType });
            dynamic myList = Activator.CreateInstance(myGenericType);
            myList.Add(message);
            
            using (var stream = new MemoryStream())
            using (var reader = new StreamReader(stream))
            {
                serializer.Serialize(myList.ToArray(), stream);
                stream.Position = 0;
                return reader.ReadToEnd();
            }
        }

        private dynamic ConstructMessageObject(Type messageType)
        {
            var defaultConstructor = messageType.GetConstructor(BindingFlags.Public | BindingFlags.Instance, null, new Type[0], null);
            dynamic message = defaultConstructor != null ? defaultConstructor.Invoke(new object[0]) : FormatterServices.GetUninitializedObject(messageType);
            
            if (message == null)
                throw new ArgumentException(string.Format("Could not create type {0}", messageType.FullName));

            return message;
        }



        private void PopulateMessageObject(dynamic message, Type nsbEncryptedString)
        {
            foreach (var prop in message.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                Type propType = prop.PropertyType;

                if (propType == typeof (string))
                    prop.SetValue(message, "some text", null);
                if (propType == nsbEncryptedString)
                {
                    dynamic encryptedString = Activator.CreateInstance(nsbEncryptedString);
                    encryptedString.Value = "some text";
                    prop.SetValue(message, encryptedString, null);
                }
                if (propType == typeof (DateTime))
                    prop.SetValue(message, DateTime.Now, null);
            }
        }
    }
}
