using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using System.Linq;

namespace MSMQHelper.Services
{
    [Serializable]
    public class NServiceBusVersion : IXmlSerializable
    {
        private string name;
        
        public static string GetBinPath()
        {
            return string.Join(";", Directory.EnumerateDirectories("NServiceBus"));
        }

        public static IEnumerable<NServiceBusVersion> GetAvailableVersions()
        {
            foreach (var directory in Directory.GetDirectories("NServiceBus"))
            {
                yield return new NServiceBusVersion(Path.GetFileName(directory));
            }
        }

        /// <summary>
        /// Required for serialization
        /// </summary>
        public NServiceBusVersion(){}

        public NServiceBusVersion(string name)
        {
            this.name = name;
        }

        public string GetPath()
        {
            return string.Format("NServiceBus\\{0}", name);
        }

        public override String ToString()
        {
            return name;
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            name = reader.ReadString();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteString(name);
        }

        public bool Equals(NServiceBusVersion other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.name, name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (NServiceBusVersion)) return false;
            return Equals((NServiceBusVersion) obj);
        }

        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        public static bool operator ==(NServiceBusVersion left, NServiceBusVersion right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(NServiceBusVersion left, NServiceBusVersion right)
        {
            return !Equals(left, right);
        }
    }
}