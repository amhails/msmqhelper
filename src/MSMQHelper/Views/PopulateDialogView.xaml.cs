﻿using System.Windows;

namespace MSMQHelper.Views
{
    /// <summary>
    /// Interaction logic for PopulateDialogView.xaml
    /// </summary>
    public partial class PopulateDialogView : Window
    {
        public PopulateDialogView()
        {
            InitializeComponent();
        }
    }
}
