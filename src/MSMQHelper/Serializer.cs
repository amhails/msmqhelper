﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace MSMQHelper
{
    public static class Serializer
    {
        public static string Serialize(Type type, object objectToSerialize)
        {
            var sb = new StringBuilder();
            var serializer = new XmlSerializer(type);
            var settings = new XmlWriterSettings { Indent = true, OmitXmlDeclaration = true };
            using (var writer = XmlWriter.Create(sb, settings))
            {
                serializer.Serialize(writer, objectToSerialize);
            }
            return sb.ToString();
        }

        public static string Serialize<T>(T objectToSerialize)
        {
            return Serialize(typeof (T), objectToSerialize);
        }

        public static T Deserialize<T>(string serializedObject)
        {
            var serializer = new XmlSerializer(typeof (T));
            using(var sr = new StringReader(serializedObject))
            using(var reader = XmlReader.Create(sr))
            {
                return (T)serializer.Deserialize(reader);
            }
        }
    }
}
