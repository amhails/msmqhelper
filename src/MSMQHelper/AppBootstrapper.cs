﻿using Autofac;
using Caliburn.Micro;
using Caliburn.Micro.Autofac;
using Caliburn.Micro.Contrib;
using Caliburn.Micro.Logging;
using MSMQHelper.ViewModels;

namespace MSMQHelper
{
    public class AppBootstrapper : AutofacBootstrapper<SendMessageViewModel>
    {
        static AppBootstrapper()
        {
            LogManager.GetLog = type => new DebugLogger(type);
        }

        protected override void Configure()
        {
            base.Configure();

            FrameworkExtensions.Message.Attach.AllowExtraSyntax(MessageSyntaxes.SpecialValueProperty |
                                                                MessageSyntaxes.XamlBinding);
            FrameworkExtensions.ActionMessage.EnableFilters();
            FrameworkExtensions.ViewLocator.EnableContextFallback();
            ViewLocator.AddSubNamespaceMapping("Caliburn.Micro.Contrib.Dialogs", "MSMQHelper.Dialogs");
        }

        protected override void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<AutofacModule>();
        }
    }
}