﻿using System.Windows;

namespace MSMQHelper.Dialogs
{
    /// <summary>
    /// Interaction logic for DialogView.xaml
    /// </summary>
    public partial class DialogView : Window
    {
        public DialogView()
        {
            InitializeComponent();
        }
    }
}
