﻿using System.Configuration;
using MSMQHelper.Services;

namespace MSMQHelper.Models
{
    [SettingsSerializeAs(SettingsSerializeAs.Xml)]
    public class UserSettings
    {
        public MessageDefinition MessageDefinition { get; set; }
        public QueueDefinition QueueDefinition { get; set; }
        public string Assembly { get; set; }
        public string EncryptionKey { get; set; }
        public NServiceBusVersion NServiceBusVersion { get; set; }
    }
}
