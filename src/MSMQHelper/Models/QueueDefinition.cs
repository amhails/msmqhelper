﻿using System.ComponentModel;
using System.Messaging;

namespace MSMQHelper.Models
{
    public class QueueDefinition : IDataErrorInfo
    {
        public string ServerName { get; set; }
        public string QueueName { get; set; }

        public string QueuePath
        {
            get { return string.Format("{0}\\private$\\{1}", ServerName, QueueName); }
        }

        public bool IsValid()
        {
            return string.IsNullOrEmpty(ValidateQueue());
        }

        public string ValidateQueue()
        {
            try
            {
                if(MessageQueue.Exists(QueuePath))
                {
                    return null;
                }
            }
            catch(MessageQueueException){}
            
            return "Queue does not exist";
        }

        string IDataErrorInfo.this[string propertyName]
        {
            get 
            { 
                return ValidateQueue();
            }
        }

        string IDataErrorInfo.Error { get { return null; } }
    }
}