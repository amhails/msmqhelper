﻿using System.ComponentModel;
using System.Xml;
using System.Xml.Linq;

namespace MSMQHelper.Models
{
    public class MessageDefinition : IDataErrorInfo
    {
        public bool IsRecoverable { get; set; }
        public string Label { get; set; }
        public string Body { get; set; }

        public bool IsValid()
        {
            return string.IsNullOrEmpty(ValidateBody()) && string.IsNullOrEmpty(ValidateLabel());
        }

        public string ValidateLabel()
        {
            if (string.IsNullOrEmpty(Label))
            {
                return "Label is required";
            }
            return null;
        }

        public string ValidateBody()
        {
            if (!string.IsNullOrEmpty(Body))
            {
                try
                {
                    XDocument.Parse(Body);
                }
                catch (XmlException)
                {
                    return "Not valid XML";
                }
            }
            return null;
        }

        string IDataErrorInfo.this[string propertyName]
        {
            get
            {
                if (propertyName == "Label")
                {
                    return ValidateLabel();
                }
                else
                {
                    return ValidateBody();    
                }
            }
        }

        string IDataErrorInfo.Error { get { return null; } }
    }
}